# Mock Server CDC

Projeto destinado a configuração das rotas utilizadas no Mock Server do ambiente de QA.

## Requisitos

Para a execução do script de configuração do Mock Server, será necessário que seu ambiente esteja configurado com:

* [Python](https://www.python.org/) >= **3.7.***

* Instalar o gerenciador de ambientes virtuais **pipenv**:

```sh
    $ pip install pipenv
```

## Executando o script para configuração do ambiente
Na raiz do projeto **mock-server-cdc/**, abrir o terminal e digitar o comando abaixo para ativar o seu ambiente virtual de desenvolvimento:
```bash
$ pipenv shell
```

Após a ativação do seu ambiente virtual, efetuar a instalação da dependência do projeto:
```bash
$ pip install -r requirements.txt
```

Após a instalação da dependência, basta executar o arquivo **environment_config.py** para configurar o ambiente:
```bash
$ python environment_config.py
```

## Fluxo de aprovação da Biometria
Após a submissão da selfie e documentos para a Biometria, a aplicação irá aguardar a chamada do webhook da IdWall, porém, sua chamada precisará ser feita de maneira manual por meio do seguinte sript:
- Substituir o valor da chave **protocolo** pelo seu número de protocolo (UUID) mockado para a IdWall (esse valor pode ser encontrado dentro do arquivo **relatories.py**). No exemplo abaixo será utilizado o número de protocolo do CPF 047.414.710-04.
- Substituir o valor **Secret-Token** no head pelo valor da variável LENDICO_CDCMANAGER_IDWALL_SECRET_TOKEN_WEBHOOK configurada no ambiente. 
```bash
def make_biometry_report_request(manager_api, cpf):
    status_list = ['PENDENTE', 'EM ANALISE', 'PROCESSANDO', 'CONCLUIDO']
    head = {
        'content-type': 'application/json',
        'Secret-Token': '***'
    }
    payload = {
        "dados": {
            "protocolo": "1fbe679d-82f7-4f07-8c17-cfa01ae215de",
            "status": "status"
        },
        "tipo": "protocolo_status"
    }
    print(f'Making requests to CPF {cpf}')
    for status in status_list:
        payload['dados']['status'] = status
        r = post(f'{manager_api}/api/biometry_report/status',
                 data=dumps(payload), headers=head)
        print(f'{status} - {r.text}')


if __name__ == '__main__':
    make_biometry_report_request(
        'https://cdc-manager.qa-titan.lendico.net.br',
        '04741471004'
    )
```
