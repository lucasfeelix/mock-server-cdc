RECUPERA_WSDL = {
    "persistent": False,
    "request": {
        "url": "/RecuperaAPI/RecuperaAPI.svc?wsdl",
        "method": "ANY"
    },
    "response": {
        "status": 200,
        "headers": {
            "Content-Type": "text/xml;charset=UTF-8"
        },
        "body": """<wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:wsx="http://schemas.xmlsoap.org/ws/2004/09/mex" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:wsa10="http://www.w3.org/2005/08/addressing" xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy" xmlns:wsap="http://schemas.xmlsoap.org/ws/2004/08/addressing/policy" xmlns:msc="http://schemas.microsoft.com/ws/2005/12/wsdl/contract" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsam="http://www.w3.org/2007/05/addressing/metadata" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://tempuri.org/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsaw="http://www.w3.org/2006/05/addressing/wsdl" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" name="Api" targetNamespace="http://tempuri.org/">
                    <script id="tinyhippos-injected"/>
                    <wsp:Policy wsu:Id="BasicHttpBinding_IApi1_policy">
                    <wsp:ExactlyOne>
                    <wsp:All>
                    <sp:TransportBinding xmlns:sp="http://schemas.xmlsoap.org/ws/2005/07/securitypolicy">
                    <wsp:Policy>
                    <sp:TransportToken>
                    <wsp:Policy>
                    <sp:HttpsToken RequireClientCertificate="false"/>
                    </wsp:Policy>
                    </sp:TransportToken>
                    <sp:AlgorithmSuite>
                    <wsp:Policy>
                    <sp:Basic256/>
                    </wsp:Policy>
                    </sp:AlgorithmSuite>
                    <sp:Layout>
                    <wsp:Policy>
                    <sp:Strict/>
                    </wsp:Policy>
                    </sp:Layout>
                    </wsp:Policy>
                    </sp:TransportBinding>
                    </wsp:All>
                    </wsp:ExactlyOne>
                    </wsp:Policy>
                    <wsdl:types>
                    <xsd:schema targetNamespace="http://tempuri.org/Imports">
                    <xsd:import schemaLocation="http://recupera.hlg.lendico.net.br:8080/RecuperaApi/RecuperaAPI.svc?xsd=xsd0" namespace="http://tempuri.org/"/>
                    <xsd:import schemaLocation="http://recupera.hlg.lendico.net.br:8080/RecuperaApi/RecuperaAPI.svc?xsd=xsd1" namespace="http://schemas.microsoft.com/2003/10/Serialization/"/>
                    <xsd:import schemaLocation="http://recupera.hlg.lendico.net.br:8080/RecuperaApi/RecuperaAPI.svc?xsd=xsd2" namespace="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models"/>
                    <xsd:import schemaLocation="http://recupera.hlg.lendico.net.br:8080/RecuperaApi/RecuperaAPI.svc?xsd=xsd3" namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
                    </xsd:schema>
                    </wsdl:types>
                    <wsdl:message name="IApi_ConsultarClientePorCPF_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarClientePorCPF"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarClientePorCPF_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarClientePorCPFResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarCliente_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarCliente"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarCliente_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarClienteResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AtualizarCliente_InputMessage">
                    <wsdl:part name="parameters" element="tns:AtualizarCliente"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AtualizarCliente_OutputMessage">
                    <wsdl:part name="parameters" element="tns:AtualizarClienteResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarContratoDetalhes_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarContratoDetalhes"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarContratoDetalhes_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarContratoDetalhesResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarParcelamentos_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarParcelamentos"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarParcelamentos_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarParcelamentosResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarDividaEmAberto_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarDividaEmAberto"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarDividaEmAberto_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarDividaEmAbertoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarSaldoAtualizado_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarSaldoAtualizado"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarSaldoAtualizado_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarSaldoAtualizadoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoComEntradaListaOpcoes_InputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoComEntradaListaOpcoes"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoComEntradaListaOpcoes_OutputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoComEntradaListaOpcoesResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoListaOpcoes_InputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoListaOpcoes"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoListaOpcoes_OutputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoListaOpcoesResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoComEntrada_InputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoComEntrada"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_SolicitarOpcoesPagamentoComEntrada_OutputMessage">
                    <wsdl:part name="parameters" element="tns:SolicitarOpcoesPagamentoComEntradaResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusSolicitacaoOpcoesPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusSolicitacaoOpcoesPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusSolicitacaoOpcoesPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusSolicitacaoOpcoesPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarOpcoesPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarOpcoesPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarOpcoesPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarOpcoesPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConfirmarOpcaoPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConfirmarOpcaoPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConfirmarOpcaoPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConfirmarOpcaoPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoPDF_InputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoPDF"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoPDF_OutputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoPDFResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoleto_InputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoleto"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoleto_OutputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoSMS_InputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoSMS"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoSMS_OutputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoSMSResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoEmail_InputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoEmail"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EmitirBoletoEmail_OutputMessage">
                    <wsdl:part name="parameters" element="tns:EmitirBoletoEmailResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AlegarPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:AlegarPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AlegarPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:AlegarPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AlegarDesconhecimentoDivida_InputMessage">
                    <wsdl:part name="parameters" element="tns:AlegarDesconhecimentoDivida"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_AlegarDesconhecimentoDivida_OutputMessage">
                    <wsdl:part name="parameters" element="tns:AlegarDesconhecimentoDividaResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EnviarPropostaPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:EnviarPropostaPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_EnviarPropostaPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:EnviarPropostaPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusSolicitacaoPropostaPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusSolicitacaoPropostaPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusSolicitacaoPropostaPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusSolicitacaoPropostaPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarPropostaPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarPropostaPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarPropostaPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarPropostaPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConfirmarPropostaPagamento_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConfirmarPropostaPagamento"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConfirmarPropostaPagamento_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConfirmarPropostaPagamentoResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusToken_InputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusToken"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ConsultarStatusToken_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ConsultarStatusTokenResponse"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ExcluirAcordo_InputMessage">
                    <wsdl:part name="parameters" element="tns:ExcluirAcordo"/>
                    </wsdl:message>
                    <wsdl:message name="IApi_ExcluirAcordo_OutputMessage">
                    <wsdl:part name="parameters" element="tns:ExcluirAcordoResponse"/>
                    </wsdl:message>
                    <wsdl:portType name="IApi">
                    <wsdl:operation name="ConsultarClientePorCPF">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarClientePorCPF" message="tns:IApi_ConsultarClientePorCPF_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarClientePorCPFResponse" message="tns:IApi_ConsultarClientePorCPF_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarCliente">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarCliente" message="tns:IApi_ConsultarCliente_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarClienteResponse" message="tns:IApi_ConsultarCliente_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="AtualizarCliente">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/AtualizarCliente" message="tns:IApi_AtualizarCliente_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/AtualizarClienteResponse" message="tns:IApi_AtualizarCliente_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarContratoDetalhes">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarContratoDetalhes" message="tns:IApi_ConsultarContratoDetalhes_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarContratoDetalhesResponse" message="tns:IApi_ConsultarContratoDetalhes_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarParcelamentos">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarParcelamentos" message="tns:IApi_ConsultarParcelamentos_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarParcelamentosResponse" message="tns:IApi_ConsultarParcelamentos_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarDividaEmAberto">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarDividaEmAberto" message="tns:IApi_ConsultarDividaEmAberto_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarDividaEmAbertoResponse" message="tns:IApi_ConsultarDividaEmAberto_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarSaldoAtualizado">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarSaldoAtualizado" message="tns:IApi_ConsultarSaldoAtualizado_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarSaldoAtualizadoResponse" message="tns:IApi_ConsultarSaldoAtualizado_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntradaListaOpcoes">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntradaListaOpcoes" message="tns:IApi_SolicitarOpcoesPagamentoComEntradaListaOpcoes_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntradaListaOpcoesResponse" message="tns:IApi_SolicitarOpcoesPagamentoComEntradaListaOpcoes_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoListaOpcoes">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoListaOpcoes" message="tns:IApi_SolicitarOpcoesPagamentoListaOpcoes_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoListaOpcoesResponse" message="tns:IApi_SolicitarOpcoesPagamentoListaOpcoes_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamento" message="tns:IApi_SolicitarOpcoesPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoResponse" message="tns:IApi_SolicitarOpcoesPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntrada">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntrada" message="tns:IApi_SolicitarOpcoesPagamentoComEntrada_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntradaResponse" message="tns:IApi_SolicitarOpcoesPagamentoComEntrada_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoOpcoesPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarStatusSolicitacaoOpcoesPagamento" message="tns:IApi_ConsultarStatusSolicitacaoOpcoesPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarStatusSolicitacaoOpcoesPagamentoResponse" message="tns:IApi_ConsultarStatusSolicitacaoOpcoesPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarOpcoesPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarOpcoesPagamento" message="tns:IApi_ConsultarOpcoesPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarOpcoesPagamentoResponse" message="tns:IApi_ConsultarOpcoesPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarOpcaoPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConfirmarOpcaoPagamento" message="tns:IApi_ConfirmarOpcaoPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConfirmarOpcaoPagamentoResponse" message="tns:IApi_ConfirmarOpcaoPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoPDF">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/EmitirBoletoPDF" message="tns:IApi_EmitirBoletoPDF_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/EmitirBoletoPDFResponse" message="tns:IApi_EmitirBoletoPDF_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoleto">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/EmitirBoleto" message="tns:IApi_EmitirBoleto_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/EmitirBoletoResponse" message="tns:IApi_EmitirBoleto_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoSMS">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/EmitirBoletoSMS" message="tns:IApi_EmitirBoletoSMS_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/EmitirBoletoSMSResponse" message="tns:IApi_EmitirBoletoSMS_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoEmail">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/EmitirBoletoEmail" message="tns:IApi_EmitirBoletoEmail_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/EmitirBoletoEmailResponse" message="tns:IApi_EmitirBoletoEmail_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/AlegarPagamento" message="tns:IApi_AlegarPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/AlegarPagamentoResponse" message="tns:IApi_AlegarPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarDesconhecimentoDivida">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/AlegarDesconhecimentoDivida" message="tns:IApi_AlegarDesconhecimentoDivida_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/AlegarDesconhecimentoDividaResponse" message="tns:IApi_AlegarDesconhecimentoDivida_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="EnviarPropostaPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/EnviarPropostaPagamento" message="tns:IApi_EnviarPropostaPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/EnviarPropostaPagamentoResponse" message="tns:IApi_EnviarPropostaPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoPropostaPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarStatusSolicitacaoPropostaPagamento" message="tns:IApi_ConsultarStatusSolicitacaoPropostaPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarStatusSolicitacaoPropostaPagamentoResponse" message="tns:IApi_ConsultarStatusSolicitacaoPropostaPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarPropostaPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarPropostaPagamento" message="tns:IApi_ConsultarPropostaPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarPropostaPagamentoResponse" message="tns:IApi_ConsultarPropostaPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarPropostaPagamento">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConfirmarPropostaPagamento" message="tns:IApi_ConfirmarPropostaPagamento_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConfirmarPropostaPagamentoResponse" message="tns:IApi_ConfirmarPropostaPagamento_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusToken">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ConsultarStatusToken" message="tns:IApi_ConsultarStatusToken_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ConsultarStatusTokenResponse" message="tns:IApi_ConsultarStatusToken_OutputMessage"/>
                    </wsdl:operation>
                    <wsdl:operation name="ExcluirAcordo">
                    <wsdl:input wsaw:Action="http://tempuri.org/IApi/ExcluirAcordo" message="tns:IApi_ExcluirAcordo_InputMessage"/>
                    <wsdl:output wsaw:Action="http://tempuri.org/IApi/ExcluirAcordoResponse" message="tns:IApi_ExcluirAcordo_OutputMessage"/>
                    </wsdl:operation>
                    </wsdl:portType>
                    <wsdl:binding name="BasicHttpBinding_IApi" type="tns:IApi">
                    <soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
                    <wsdl:operation name="ConsultarClientePorCPF">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarClientePorCPF" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarCliente">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarCliente" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AtualizarCliente">
                    <soap:operation soapAction="http://tempuri.org/IApi/AtualizarCliente" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarContratoDetalhes">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarContratoDetalhes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarParcelamentos">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarParcelamentos" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarDividaEmAberto">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarDividaEmAberto" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarSaldoAtualizado">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarSaldoAtualizado" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntradaListaOpcoes">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntradaListaOpcoes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoListaOpcoes">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoListaOpcoes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntrada">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntrada" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusSolicitacaoOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarOpcaoPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConfirmarOpcaoPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoPDF">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoPDF" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoleto">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoleto" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoSMS">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoSMS" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoEmail">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoEmail" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/AlegarPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarDesconhecimentoDivida">
                    <soap:operation soapAction="http://tempuri.org/IApi/AlegarDesconhecimentoDivida" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EnviarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/EnviarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusSolicitacaoPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConfirmarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusToken">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusToken" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ExcluirAcordo">
                    <soap:operation soapAction="http://tempuri.org/IApi/ExcluirAcordo" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    </wsdl:binding>
                    <wsdl:binding name="BasicHttpBinding_IApi1" type="tns:IApi">
                    <wsp:PolicyReference URI="#BasicHttpBinding_IApi1_policy"/>
                    <soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
                    <wsdl:operation name="ConsultarClientePorCPF">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarClientePorCPF" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarCliente">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarCliente" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AtualizarCliente">
                    <soap:operation soapAction="http://tempuri.org/IApi/AtualizarCliente" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarContratoDetalhes">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarContratoDetalhes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarParcelamentos">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarParcelamentos" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarDividaEmAberto">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarDividaEmAberto" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarSaldoAtualizado">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarSaldoAtualizado" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntradaListaOpcoes">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntradaListaOpcoes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoListaOpcoes">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoListaOpcoes" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="SolicitarOpcoesPagamentoComEntrada">
                    <soap:operation soapAction="http://tempuri.org/IApi/SolicitarOpcoesPagamentoComEntrada" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusSolicitacaoOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarOpcoesPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarOpcoesPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarOpcaoPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConfirmarOpcaoPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoPDF">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoPDF" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoleto">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoleto" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoSMS">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoSMS" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EmitirBoletoEmail">
                    <soap:operation soapAction="http://tempuri.org/IApi/EmitirBoletoEmail" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/AlegarPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="AlegarDesconhecimentoDivida">
                    <soap:operation soapAction="http://tempuri.org/IApi/AlegarDesconhecimentoDivida" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="EnviarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/EnviarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusSolicitacaoPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusSolicitacaoPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConfirmarPropostaPagamento">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConfirmarPropostaPagamento" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ConsultarStatusToken">
                    <soap:operation soapAction="http://tempuri.org/IApi/ConsultarStatusToken" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    <wsdl:operation name="ExcluirAcordo">
                    <soap:operation soapAction="http://tempuri.org/IApi/ExcluirAcordo" style="document"/>
                    <wsdl:input>
                    <soap:body use="literal"/>
                    </wsdl:input>
                    <wsdl:output>
                    <soap:body use="literal"/>
                    </wsdl:output>
                    </wsdl:operation>
                    </wsdl:binding>
                    <wsdl:service name="Api">
                    <wsdl:port name="BasicHttpBinding_IApi" binding="tns:BasicHttpBinding_IApi">
                    <soap:address location="https://mock-server.qa-titan.lendico.net.br/RecuperaApi/RecuperaAPI.svc"/>
                    </wsdl:port>
                    <wsdl:port name="BasicHttpBinding_IApi1" binding="tns:BasicHttpBinding_IApi1">
                    <soap:address location="https://ec2amaz-7jflhid:55443/RecuperaApi/RecuperaAPI.svc"/>
                    </wsdl:port>
                    </wsdl:service>
                    </wsdl:definitions>"""
    }
}

RECUPERA_ConsultarDividaEmAberto = {
    "persistent": False,
    "request": {
        "method": "POST",
        "url": "/RecuperaApi/RecuperaAPI.svc",
        "bodyPatterns": [
            {"contains": 'ConsultarDividaEmAberto'},
            {
                "contains": '<ns4:CodigoCliente xmlns:ns4="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models">04741471004</ns4:CodigoCliente>'}
        ]
    },
    "response": {
        "status": 200,
        "headers": {
            "Content-Type": "text/xml;charset=UTF-8"
        },
        "body": """<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                        <s:Body>
                            <ConsultarDividaEmAbertoResponse xmlns="http://tempuri.org/">
                                <ConsultarDividaEmAbertoResult xmlns:a="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                    <a:Contratos>
                                        <a:Contrato>
                                            <a:CodigoContrato>901082660</a:CodigoContrato>
                                            <a:CodigoProduto>1</a:CodigoProduto>
                                            <a:Prestacoes>
                                                <a:Prestacao>
                                                    <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
                                                    <a:DataVencimento>2020-07-21T00:00:00</a:DataVencimento>
                                                    <a:IdPrestacao>20200721</a:IdPrestacao>
                                                    <a:NumeroPrestacao>001</a:NumeroPrestacao>
                                                    <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
                                                    <a:ValorDesconto>2.34</a:ValorDesconto>
                                                    <a:ValorJuros>0</a:ValorJuros>
                                                    <a:ValorMulta>0</a:ValorMulta>
                                                    <a:ValorPagamento>133.59</a:ValorPagamento>
                                                    <a:ValorPrincipal>135.93</a:ValorPrincipal>
                                                    <a:ValorReceita>0</a:ValorReceita>
                                                    <a:ValorRepasse>133.59</a:ValorRepasse>
                                                    <a:ValorTxAdm>0</a:ValorTxAdm>
                                                </a:Prestacao>
                                                <a:Prestacao>
                                                    <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
                                                    <a:DataVencimento>2020-08-21T00:00:00</a:DataVencimento>
                                                    <a:IdPrestacao>20200821</a:IdPrestacao>
                                                    <a:NumeroPrestacao>002</a:NumeroPrestacao>
                                                    <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
                                                    <a:ValorDesconto>5.72</a:ValorDesconto>
                                                    <a:ValorJuros>0</a:ValorJuros>
                                                    <a:ValorMulta>0</a:ValorMulta>
                                                    <a:ValorPagamento>130.21</a:ValorPagamento>
                                                    <a:ValorPrincipal>135.93</a:ValorPrincipal>
                                                    <a:ValorReceita>0</a:ValorReceita>
                                                    <a:ValorRepasse>130.21</a:ValorRepasse>
                                                    <a:ValorTxAdm>0</a:ValorTxAdm>
                                                </a:Prestacao>
                                                <a:Prestacao>
                                                    <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
                                                    <a:DataVencimento>2020-09-21T00:00:00</a:DataVencimento>
                                                    <a:IdPrestacao>20200921</a:IdPrestacao>
                                                    <a:NumeroPrestacao>003</a:NumeroPrestacao>
                                                    <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
                                                    <a:ValorDesconto>9.02</a:ValorDesconto>
                                                    <a:ValorJuros>0</a:ValorJuros>
                                                    <a:ValorMulta>0</a:ValorMulta>
                                                    <a:ValorPagamento>126.91</a:ValorPagamento>
                                                    <a:ValorPrincipal>135.93</a:ValorPrincipal>
                                                    <a:ValorReceita>0</a:ValorReceita>
                                                    <a:ValorRepasse>126.91</a:ValorRepasse>
                                                    <a:ValorTxAdm>0</a:ValorTxAdm>
                                                </a:Prestacao>
                                            </a:Prestacoes>
                                            <a:ValorDesconto>17.08</a:ValorDesconto>
                                            <a:ValorJuros>0</a:ValorJuros>
                                            <a:ValorMulta>0</a:ValorMulta>
                                            <a:ValorPagamento>390.71</a:ValorPagamento>
                                            <a:ValorPrincipal>407.79</a:ValorPrincipal>
                                            <a:ValorReceita>0</a:ValorReceita>
                                            <a:ValorRepasse>390.71</a:ValorRepasse>
                                            <a:ValorTxAdm>0</a:ValorTxAdm>
                                            <a:ValorTxContrato>0</a:ValorTxContrato>
                                        </a:Contrato>
                                    </a:Contratos>
                                    <a:StatusRetorno>
                                        <a:CodigoRetorno>0</a:CodigoRetorno>
                                        <a:MensagemRetorno>Consulta de dívida em aberto realizada.</a:MensagemRetorno>
                                    </a:StatusRetorno>
                                </ConsultarDividaEmAbertoResult>
                            </ConsultarDividaEmAbertoResponse>
                        </s:Body>
                        </s:Envelope>"""
    }
}

RECUPERA_ConsultarParcelamentos = {
    "persistent": False,
    "request": {
        "method": "POST",
        "url": "/RecuperaApi/RecuperaAPI.svc",
        "bodyPatterns": [
            {"contains": 'ConsultarParcelamentos'},
            {
                "contains": '<ns4:CodigoCliente xmlns:ns4="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models">04741471004</ns4:CodigoCliente>'}
        ]
    },
    "response": {
        "status": 200,
        "headers": {
            "Content-Type": "text/xml;charset=UTF-8"
        },
        "body": """<s:Envelope
                    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                    <s:Body>
                        <ConsultarParcelamentosResponse
                            xmlns="http://tempuri.org/">
                            <ConsultarParcelamentosResult
                                xmlns:a="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models"
                                xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                <a:Parcelamentos i:nil="true"/>
                                <a:StatusRetorno>
                                    <a:CodigoRetorno>15</a:CodigoRetorno>
                                    <a:MensagemRetorno>Erro ao Consultar Parcelamento em aberto. Este cliente n&#227;o possui acordos em aberto</a:MensagemRetorno>
                                </a:StatusRetorno>
                            </ConsultarParcelamentosResult>
                        </ConsultarParcelamentosResponse>
                    </s:Body>
                </s:Envelope>"""
    }
}

# RECUPERA_ConsultarDividaEmAberto = {
#     "persistent": False,
#     "request": {
#         "method": "POST",
#         "url": "/RecuperaApi/RecuperaAPI.svc",
#         "bodyPatterns": [
#             {"contains": 'ConsultarDividaEmAberto'},
#             {"contains": '<ns4:CodigoCliente xmlns:ns4="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models">04741471004</ns4:CodigoCliente>'}
#         ]
#     },
#     "response": {
#         "status": 200,
#         "headers": {
#             "Content-Type": "text/xml;charset=UTF-8"
#         },
#         "body": """<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
#                     <s:Body>
#                         <ConsultarDividaEmAbertoResponse xmlns="http://tempuri.org/">
#                             <ConsultarDividaEmAbertoResult xmlns:a="http://schemas.datacontract.org/2004/07/RecuperaAPI.Models" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
#                                 <a:Contratos>
#                                     <a:Contrato>
#                                         <a:CodigoContrato>901082660</a:CodigoContrato>
#                                         <a:CodigoProduto>1</a:CodigoProduto>
#                                         <a:Prestacoes>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-07-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200721</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>001</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>2.34</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>133.59</a:ValorPagamento>
#                                                 <a:ValorPrincipal>135.93</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>133.59</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-08-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200821</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>002</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>5.72</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>130.21</a:ValorPagamento>
#                                                 <a:ValorPrincipal>135.93</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>130.21</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-09-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200921</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>003</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>9.02</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>126.91</a:ValorPagamento>
#                                                 <a:ValorPrincipal>135.93</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>126.91</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                         </a:Prestacoes>
#                                         <a:ValorDesconto>17.08</a:ValorDesconto>
#                                         <a:ValorJuros>0</a:ValorJuros>
#                                         <a:ValorMulta>0</a:ValorMulta>
#                                         <a:ValorPagamento>390.71</a:ValorPagamento>
#                                         <a:ValorPrincipal>407.79</a:ValorPrincipal>
#                                         <a:ValorReceita>0</a:ValorReceita>
#                                         <a:ValorRepasse>390.71</a:ValorRepasse>
#                                         <a:ValorTxAdm>0</a:ValorTxAdm>
#                                         <a:ValorTxContrato>0</a:ValorTxContrato>
#                                     </a:Contrato>
#                                     <a:Contrato>
#                                         <a:CodigoContrato>4741471004</a:CodigoContrato>
#                                         <a:CodigoProduto>2</a:CodigoProduto>
#                                         <a:Prestacoes>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-07-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200721</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>001</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>20.1</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>1163.84</a:ValorPagamento>
#                                                 <a:ValorPrincipal>1183.94</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>1163.84</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-08-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200821</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>002</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>49.16</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>1134.78</a:ValorPagamento>
#                                                 <a:ValorPrincipal>1183.94</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>1134.78</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                             <a:Prestacao>
#                                                 <a:DataCorrecao>0001-01-01T00:00:00</a:DataCorrecao>
#                                                 <a:DataVencimento>2020-09-21T00:00:00</a:DataVencimento>
#                                                 <a:IdPrestacao>20200921</a:IdPrestacao>
#                                                 <a:NumeroPrestacao>003</a:NumeroPrestacao>
#                                                 <a:StatusPrestacao>EmAberto</a:StatusPrestacao>
#                                                 <a:ValorDesconto>77.48</a:ValorDesconto>
#                                                 <a:ValorJuros>0</a:ValorJuros>
#                                                 <a:ValorMulta>0</a:ValorMulta>
#                                                 <a:ValorPagamento>1106.46</a:ValorPagamento>
#                                                 <a:ValorPrincipal>1183.94</a:ValorPrincipal>
#                                                 <a:ValorReceita>0</a:ValorReceita>
#                                                 <a:ValorRepasse>1106.46</a:ValorRepasse>
#                                                 <a:ValorTxAdm>0</a:ValorTxAdm>
#                                             </a:Prestacao>
#                                         </a:Prestacoes>
#                                         <a:ValorDesconto>146.74</a:ValorDesconto>
#                                         <a:ValorJuros>0</a:ValorJuros>
#                                         <a:ValorMulta>0</a:ValorMulta>
#                                         <a:ValorPagamento>3405.08</a:ValorPagamento>
#                                         <a:ValorPrincipal>3551.82</a:ValorPrincipal>
#                                         <a:ValorReceita>0</a:ValorReceita>
#                                         <a:ValorRepasse>3405.08</a:ValorRepasse>
#                                         <a:ValorTxAdm>0</a:ValorTxAdm>
#                                         <a:ValorTxContrato>0</a:ValorTxContrato>
#                                     </a:Contrato>
#                                 </a:Contratos>
#                                 <a:StatusRetorno>
#                                     <a:CodigoRetorno>0</a:CodigoRetorno>
#                                     <a:MensagemRetorno>Consulta de dívida em aberto realizada.</a:MensagemRetorno>
#                                 </a:StatusRetorno>
#                             </ConsultarDividaEmAbertoResult>
#                         </ConsultarDividaEmAbertoResponse>
#                     </s:Body>
#                 </s:Envelope>"""
#     }
# }
