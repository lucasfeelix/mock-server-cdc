CALLBACK_URL = {
    "persistent": 'true',
    "request": {
        "url": "/qa-callback",
        "method": "POST",
        "bodyPatterns": [{
            "equalTo": "{\"transactionId\": \"D3AA1FC8372E430E8236649DB5EBD08G\"}"
            # NOQA
        }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "message": "OK"
        }
    }
}
