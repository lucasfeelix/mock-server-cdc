ID_04741471004 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '04741471004')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "1fbe679d-82f7-4f07-8c17-cfa01ae215de"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_04741471004 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/1fbe679d-82f7-4f07-8c17-cfa01ae215de/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "1fbe679d-82f7-4f07-8c17-cfa01ae215de",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "04741471004",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "04741471004",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_04741471004_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '04741471004')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "0da36167-35b3-48d9-96b1-86ead7f0d7ca"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_04741471004_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/0da36167-35b3-48d9-96b1-86ead7f0d7ca/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "0da36167-35b3-48d9-96b1-86ead7f0d7ca",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_19924764030 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '19924764030')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "97fec609-3924-41d3-acfb-5b03403ba693"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_19924764030 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/97fec609-3924-41d3-acfb-5b03403ba693/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Inválido.",
                "nome": "consultaPessoaDefault",
                "numero": "97fec609-3924-41d3-acfb-5b03403ba693",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "19924764030",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "INVALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "19924764030",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_19924764030_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '19924764030')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "f1be6122-c613-4d74-9979-abc643eae9a8"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_19924764030_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/f1be6122-c613-4d74-9979-abc643eae9a8/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "f1be6122-c613-4d74-9979-abc643eae9a8",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_23158907054 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '23158907054')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "bade26b9-2fa4-4769-a71c-80ec08bbe5f9"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_23158907054 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/bade26b9-2fa4-4769-a71c-80ec08bbe5f9/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Inválido.",
                "nome": "consultaPessoaDefault",
                "numero": "bade26b9-2fa4-4769-a71c-80ec08bbe5f9",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "23158907054",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "INVALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "51028662653",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_23158907054_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '23158907054')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "06a2d9b6-80ba-4efa-b346-cf7048af2a9b"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_23158907054_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/06a2d9b6-80ba-4efa-b346-cf7048af2a9b/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "06a2d9b6-80ba-4efa-b346-cf7048af2a9b",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_61829708040 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '61829708040')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "b006d35d-9a23-4468-9d46-d0edaa96df5b"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_61829708040 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/b006d35d-9a23-4468-9d46-d0edaa96df5b/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "b006d35d-9a23-4468-9d46-d0edaa96df5b",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "61829708040",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "61829708040",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_61829708040_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '61829708040')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "37b42def-9ca6-4757-be4a-b8db69fbf5be"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_61829708040_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/37b42def-9ca6-4757-be4a-b8db69fbf5be/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "37b42def-9ca6-4757-be4a-b8db69fbf5be",
                "resultado": "MANUAL_REPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_BIO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '47305081060')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "465a8266-6f3d-4952-a774-49244b8082de"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_BIO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/465a8266-6f3d-4952-a774-49244b8082de/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "465a8266-6f3d-4952-a774-49244b8082de",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "47305081060",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "47305081060",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_BIO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '47305081060')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "d0df54f9-777c-475c-97de-00ad5745b5ad"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_BIO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/d0df54f9-777c-475c-97de-00ad5745b5ad/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "d0df54f9-777c-475c-97de-00ad5745b5ad",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_33176053088 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '33176053088')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "ed7350be-8bed-4a0a-aad5-2478312672bb"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_33176053088 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/ed7350be-8bed-4a0a-aad5-2478312672bb/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "ed7350be-8bed-4a0a-aad5-2478312672bb",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "33176053088",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "33176053088",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_33176053088_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '33176053088')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "c7db05b5-808c-42d4-b9ef-c649d0d20f2a"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_33176053088_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/c7db05b5-808c-42d4-b9ef-c649d0d20f2a/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "c7db05b5-808c-42d4-b9ef-c649d0d20f2a",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_15260070020 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '15260070020')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "53794cd1-e9e2-4c18-9f60-b5d14c1f618a"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_15260070020 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/53794cd1-e9e2-4c18-9f60-b5d14c1f618a/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "53794cd1-e9e2-4c18-9f60-b5d14c1f618a",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "15260070020",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "15260070020",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_15260070020_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '15260070020')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "3485e49d-3157-4779-8926-a86184cc03e3"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_15260070020_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/3485e49d-3157-4779-8926-a86184cc03e3/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "3485e49d-3157-4779-8926-a86184cc03e3",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_LUCAS = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '47039158809')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "c371ee53-d144-4471-851b-1b99d30ffa04"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_LUCAS = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/c371ee53-d144-4471-851b-1b99d30ffa04/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "c371ee53-d144-4471-851b-1b99d30ffa04",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "47039158809",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "47039158809",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_LUCAS_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '47039158809')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "55431fe7-80f0-423e-9f52-a6f7a48131f0"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_LUCAS_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/55431fe7-80f0-423e-9f52-a6f7a48131f0/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "55431fe7-80f0-423e-9f52-a6f7a48131f0",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_LEANDRO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '88333078168')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "3df7b219-bcc6-454e-bcf0-0c8b3bfe938f"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_LEANDRO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/3df7b219-bcc6-454e-bcf0-0c8b3bfe938f/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "3df7b219-bcc6-454e-bcf0-0c8b3bfe938f",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "88333078168",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "88333078168",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_LEANDRO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '88333078168')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "431c97a2-6336-476a-a19e-6f63d2d3f61f"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_LEANDRO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/431c97a2-6336-476a-a19e-6f63d2d3f61f/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "431c97a2-6336-476a-a19e-6f63d2d3f61f",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_SAULO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '43404896890')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "b03e2b94-1205-42e6-800e-5bc4cb9222f8"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_SAULO = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/b03e2b94-1205-42e6-800e-5bc4cb9222f8/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "b03e2b94-1205-42e6-800e-5bc4cb9222f8",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "43404896890",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "43404896890",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_SAULO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [
            {
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '43404896890')]"
            },
            {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }
        ]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "2fcd536e-b911-44d1-8527-b41e19d73ca4"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_SAULO_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/2fcd536e-b911-44d1-8527-b41e19d73ca4/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "2fcd536e-b911-44d1-8527-b41e19d73ca4",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_ANDRE = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '13642821863')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "5d9f1bfe-f81c-4fa4-9d65-4d4ed426c138"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_ANDRE = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/5d9f1bfe-f81c-4fa4-9d65-4d4ed426c138/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "5d9f1bfe-f81c-4fa4-9d65-4d4ed426c138",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "13642821863",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "13642821863",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_ANDRE_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '13642821863')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "11e9e8dc-72fa-4fdf-8c92-4fccf5d33ae5"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_ANDRE_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/11e9e8dc-72fa-4fdf-8c92-4fccf5d33ae5/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "11e9e8dc-72fa-4fdf-8c92-4fccf5d33ae5",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_81008489042 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '81008489042')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "ae86f4c4-2d4a-4898-be7b-f7ef313d9989"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_81008489042 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/ae86f4c4-2d4a-4898-be7b-f7ef313d9989/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "ae86f4c4-2d4a-4898-be7b-f7ef313d9989",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "81008489042",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "81008489042",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_81008489042_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '81008489042')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "9561bb26-2542-43f5-8bec-b804e1cc8f17"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_81008489042_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/9561bb26-2542-43f5-8bec-b804e1cc8f17/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "9561bb26-2542-43f5-8bec-b804e1cc8f17",
                "resultado": "INVALID",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

############################### USER TEST ######################################

ID_28744717040 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '28744717040')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "da949eed-5b7e-4df5-abaf-5f7238205590"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_28744717040 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/da949eed-5b7e-4df5-abaf-5f7238205590/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "da949eed-5b7e-4df5-abaf-5f7238205590",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "28744717040",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "28744717040",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_28744717040_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '28744717040')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "07ee87e7-d4fc-4d6e-9a6e-985b3b570540"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_28744717040_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/07ee87e7-d4fc-4d6e-9a6e-985b3b570540/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "07ee87e7-d4fc-4d6e-9a6e-985b3b570540",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

############################### USER TEST ######################################

ID_91307888038 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '91307888038')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm.*/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "34c800dc-d385-416d-affc-1ede6bb553cf"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_91307888038 = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/34c800dc-d385-416d-affc-1ede6bb553cf/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "34c800dc-d385-416d-affc-1ede6bb553cf",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "91307888038",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "91307888038",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_91307888038_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '91307888038')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "6091b64e-690c-413c-87a4-1650fd12e978"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_91307888038_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/6091b64e-690c-413c-87a4-1650fd12e978/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "6091b64e-690c-413c-87a4-1650fd12e978",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################

ID_65476005056_CNH = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '65476005056')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm_cnh_rf/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "16d74fdd-0afe-4179-a2f9-95e3d1c26207"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_65476005056_CNH = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/16d74fdd-0afe-4179-a2f9-95e3d1c26207/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Inválido.",
                "nome": "consultaPessoaDefault",
                "numero": "16d74fdd-0afe-4179-a2f9-95e3d1c26207",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "65476005056",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "INVALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "65476005056",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_65476005056_RG = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '65476005056')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz =~ /lendico_ocr_fm_rg/)]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "22398a30-ecd6-454f-adfb-d15a1b779266"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_65476005056_RG = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/22398a30-ecd6-454f-adfb-d15a1b779266/parametros",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "22398a30-ecd6-454f-adfb-d15a1b779266",
                "parametros": {
                    "cnh": "12345678900",
                    "cpf": "65476005056",
                    "data_de_nascimento": "01/01/2001",
                    "imagens_facematch": [
                        {
                            "url": "chickenonaraft.com/",
                            "principal": False
                        },
                        {
                            "url": "chickenonaraft.com/",
                            "principal": True
                        }
                    ],
                    "nome": "JOAO SILVA"
                },
                "resultado": "VALID",
                "status": "CONCLUIDO",
                "documento_ocr": {
                    "filename_front": "hemansings.com/",
                    "id_usuario": "1234",
                    "status": "OK",
                    "nome": "JOAO SILVA",
                    "rg": None,
                    "orgao_emissor_rg": None,
                    "estado_emissao_rg": None,
                    "cpf": "65476005056",
                    "data_de_nascimento": "01/01/2001",
                    "nome_do_pai": None,
                    "nome_da_mae": None,
                    "categoria": None,
                    "numero_registro": "98765432100",
                    "validade": None,
                    "data_primeira_habilitacao": None,
                    "observacoes": None,
                    "data_de_emissao": None,
                    "numero_renach": None,
                    "numero_espelho": None,
                    "permissao": None,
                    "orgao_emissor": None,
                    "local_emissao": None,
                    "estado_emissao": None,
                    "acc": None,
                    "numero": "e9fb5e0df846dfee53f3915dd8028cbb",
                    "numero_seguranca": None,
                    "filename_back": "hemansings.com/",
                    "matriz": "consultaPessoaDefault",
                    "id_protocolo": "789456321",
                    "foto_perfil": None,
                    "rg_digito": None,
                    "filename_full": None,
                    "mask": False
                }
            },
            "status_code": 200
        }
    }
}

ID_65476005056_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios",
        "method": "POST",
        "bodyPatterns": [{
                "matchesJsonPath": "$.parametros[?(@.cpf_numero == '65476005056')]"
            }, {
                "matchesJsonPath": "$[?(@.matriz == 'tipificacao_lendico')]"
            }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {"numero": "f8e6beda-5ea3-447a-a8a6-be475aadd372"},
            "status_code": 200
        }
    }
}

ID_RESPONSE_65476005056_DOCUMENTOSCOPIA = {
    "persistent": 'true',
    "request": {
        "url": "/relatorios/f8e6beda-5ea3-447a-a8a6-be475aadd372/dados",
        "method": "GET",
        "bodyPatterns": []
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "result": {
                "atualizado_em": "2020-02-19T18:00:43.391Z",
                "mensagem": "Válido.",
                "nome": "consultaPessoaDefault",
                "numero": "f8e6beda-5ea3-447a-a8a6-be475aadd372",
                "resultado": "MANUAL_APPROVAL",
                "status": "CONCLUIDO",
            },
            "status_code": 200
        }
    }
}

################################################################################
