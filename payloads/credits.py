CREDIT_04741471004 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>04741471004</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>04741471004</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_28157122023 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>28157122023</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>28157122023</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>100</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_72703627009 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>72703627009</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>72703627009</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_19924764030 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>19924764030</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>19924764030</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_23158907054 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>23158907054</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>23158907054</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_61829708040 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>61829708040</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>61829708040</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_81008489042 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>81008489042</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>81008489042</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_A = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>15260070020</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>15260070020</tr_500:documento>
                    <tr_500:nome>CARLOS DA SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>750</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_B = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>04741645002</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>04741645002</tr_500:documento>
                    <tr_500:nome>CARLOS DA SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>550</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_C = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>33176053088</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>33176053088</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>400</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_D = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>04327142000</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>04327142000</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>180</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_E = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>12236457030</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>12236457030</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>100</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_A_EMAIL = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>64027405005</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>64027405005</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>750</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_B_EMAIL = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>68257728055</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>68257728055</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>550</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_C_EMAIL = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>35527794001</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>35527794001</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>11/02/1988</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>400</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_BIO = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>47305081060</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>47305081060</tr_500:documento>
                    <tr_500:nome>CARLOS SILVA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>11</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>400</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 3.100</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_LUCAS = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>47039158809</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>47039158809</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_LEANDRO = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>88333078168</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>88333078168</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_ANDRE = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>13642821863</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>13642821863</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_MARCUS = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>38256377860</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>38256377860</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_SAULO = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>43404896890</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>43404896890</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_DESIGN_1 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>90757645100</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>90757645100</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_SCORE_DESIGN_2 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>35243884855</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>35243884855</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_65476005056 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>65476005056</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>65476005056</tr_500:documento>
                    <tr_500:nome>JOSÉ ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

# USER TEST

CREDIT_28744717040 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>28744717040</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>28744717040</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_91307888038 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>91307888038</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>91307888038</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_03724749007 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>03724749007</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>03724749007</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_04822044009 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>04822044009</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>04822044009</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

# MESA

CREDIT_MESA_1 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>27042696883</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>27042696883</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_MESA_2 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>19072463803</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>19072463803</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_MESA_3 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>01395189323</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>01395189323</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

CREDIT_MESA_4 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>18602653832</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <ns29:essencial
                xmlns:tr_500="http://boavistaservicos.com.br/familia/acerta/pf/identificacao"
                xmlns:tr_501="http://boavistaservicos.com.br/familia/acerta/pf/localizacao"
                xmlns:tr_601="http://boavistaservicos.com.br/familia/acerta/pf/score_classificacao_varios_modelos"
                xmlns:ns29="http://boavistaservicos.com.br/familia/acerta/pf/essencial/v3">
                <tr_500:identificacao>
                    <tr_500:tamanhoRegistro>316</tr_500:tamanhoRegistro>
                    <tr_500:tipoRegistro>500</tr_500:tipoRegistro>
                    <tr_500:registro>S</tr_500:registro>
                    <tr_500:documento>18602653832</tr_500:documento>
                    <tr_500:nome>ANDERSON COSTA</tr_500:nome>
                    <tr_500:nomeMae>-</tr_500:nomeMae>
                    <tr_500:dataNascimento>22/05/1992</tr_500:dataNascimento>
                    <tr_500:numeroRG></tr_500:numeroRG>
                    <tr_500:orgaoEmissor></tr_500:orgaoEmissor>
                    <tr_500:unidadeFedarativaRG></tr_500:unidadeFedarativaRG>
                    <tr_500:dataEmissaoRG>-</tr_500:dataEmissaoRG>
                    <tr_500:sexoConsultado>1</tr_500:sexoConsultado>
                    <tr_500:cidadeNascimento></tr_500:cidadeNascimento>
                    <tr_500:estadoCivil>0</tr_500:estadoCivil>
                    <tr_500:numeroDependentes>0</tr_500:numeroDependentes>
                    <tr_500:grauInstrucao>7</tr_500:grauInstrucao>
                    <tr_500:situacaoReceita>1</tr_500:situacaoReceita>
                    <tr_500:dataAtualizacao>12/08/2019</tr_500:dataAtualizacao>
                    <tr_500:regiaoCpf>8</tr_500:regiaoCpf>
                    <tr_500:tituloEleitor>0000000000000</tr_500:tituloEleitor>
                    <tr_500:obito></tr_500:obito>
                </tr_500:identificacao>
                <tr_501:localizacao>
                    <tr_501:tamanhoRegistro>323</tr_501:tamanhoRegistro>
                    <tr_501:tipoRegistro>501</tr_501:tipoRegistro>
                    <tr_501:registro>S</tr_501:registro>
                    <tr_501:tipoLogradouro>R</tr_501:tipoLogradouro>
                    <tr_501:nomeLogradouro>AV. MOFARREJ</tr_501:nomeLogradouro>
                    <tr_501:numeroLogradouro>825</tr_501:numeroLogradouro>
                    <tr_501:complemento>GÃLPÃO</tr_501:complemento>
                    <tr_501:bairro>VILA LEOPOLDINA</tr_501:bairro>
                    <tr_501:cidade>SÃO PAULO</tr_501:cidade>
                    <tr_501:unidadeFederativa>SP</tr_501:unidadeFederativa>
                    <tr_501:cep>05311000</tr_501:cep>
                    <tr_501:ddd_1>011</tr_501:ddd_1>
                    <tr_501:telefone_1>912345678</tr_501:telefone_1>
                    <tr_501:ddd_2></tr_501:ddd_2>
                    <tr_501:telefone_2></tr_501:telefone_2>
                    <tr_501:ddd_3></tr_501:ddd_3>
                    <tr_501:telefone_3></tr_501:telefone_3>
                </tr_501:localizacao>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>748</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>63</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>63</tr_601:modeloScore>
                    <tr_601:nomeScore>NOVO RISCO 6 MESES</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>02</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica>A</tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00740</tr_601:probabilidade>
                    <tr_601:texto>De cada 100 pessoas classificadas nesta classe de score, é provável que 7 apresentem débitos no mercado nos próximos 6 meses.</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>101</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>CREDITO</tr_601:descricaoNatureza>
                    <tr_601:texto2></tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
                <tr_601:score_classificacao_varios_modelos>
                    <tr_601:tamanhoRegistro>450</tr_601:tamanhoRegistro>
                    <tr_601:tipoRegistro>601</tr_601:tipoRegistro>
                    <tr_601:registro>S</tr_601:registro>
                    <tr_601:tipoScore>1</tr_601:tipoScore>
                    <tr_601:score>3</tr_601:score>
                    <tr_601:planoExecucao>N</tr_601:planoExecucao>
                    <tr_601:modeloPlano>41</tr_601:modeloPlano>
                    <tr_601:nomePlano></tr_601:nomePlano>
                    <tr_601:modeloScore>41</tr_601:modeloScore>
                    <tr_601:nomeScore>Renda Presum Faixa</tr_601:nomeScore>
                    <tr_601:classificacaoNumerica>03</tr_601:classificacaoNumerica>
                    <tr_601:classificacaoAlfabetica></tr_601:classificacaoAlfabetica>
                    <tr_601:probabilidade>00000</tr_601:probabilidade>
                    <tr_601:texto>De R$ 801 até R$ 1.400</tr_601:texto>
                    <tr_601:codigoNaturezaModelo>103</tr_601:codigoNaturezaModelo>
                    <tr_601:descricaoNatureza>RENDA PRESUMIDA</tr_601:descricaoNatureza>
                    <tr_601:texto2>Renda estimada a partir de informações comportamentais e cadastrais do consumidor.</tr_601:texto2>
                </tr_601:score_classificacao_varios_modelos>
            </ns29:essencial>"""
    }
}

# TESTE BVS

CREDIT_37541438081 = {
    "persistent": True,
    "request": {
        "url": "/FamiliaAcertaPFXmlWeb/essencial/v3",
        "method": "POST",
        "bodyPatterns": [{
            "equalToXml": '<acertaContratoEntrada xmlns="http://boavistaservicos.com.br/familia/acerta/pf"><usuario>${xmlunit.ignore}</usuario><senha>${xmlunit.ignore}</senha><cpf>37541438081</cpf><tipoCredito>CC</tipoCredito><consultaChequeSimples>S</consultaChequeSimples></acertaContratoEntrada>',
            "enablePlaceholders": True
        }]
    },
    "response": {
        "status": 200,
        "body": """<?xml version="1.0" standalone="yes"?>
            <mensagem>
                <codigo_erro>011</codigo_erro>
                <mensagem_erro>Servico Indisponivel.</mensagem_erro>
            </mensagem>"""
    }
}
