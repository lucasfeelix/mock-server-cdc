NF_04741471004 = {
    "persistent": 'true',
    "request": {
        "url": "/get-order-linx",
        "method": "POST",
        "bodyPatterns": [{
            "equalTo": "\"4a97c3cc-3180-4f44-905b-b4e9e2caa5e0\""
        }]
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "OrderInvoice": {
                "Url": "http://www.omfgdogs.com/",
                "Code": "ABCD1234",
                "OrderInvoiceID": "12345678"
            },
            "otherData": "DataData",
            "oneMoreData": "DataData"
        }
    }
}

POST_HEALTH_LINX_ = {
    "persistent": True,
    "request": {
        "url": "/health-linx",
        "method": "POST",
        "basicAuth": {
            "username": "test-user",
            "password": "test-password"
        }
    },
    "response": {
        "status": 200,
        "jsonBody": {
            "Errors": [],
            "IsValid": True,
            "Warnings": None
        }
    }
}
