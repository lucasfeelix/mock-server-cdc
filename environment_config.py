from requests import delete, get, post
from json import dumps, loads

from payloads.callback import CALLBACK_URL
from payloads.credits import (CREDIT_04741471004, CREDIT_28157122023,
                              CREDIT_72703627009, CREDIT_23158907054,
                              CREDIT_19924764030, CREDIT_SCORE_A,
                              CREDIT_SCORE_B, CREDIT_SCORE_C, CREDIT_SCORE_D,
                              CREDIT_SCORE_E, CREDIT_SCORE_A_EMAIL,
                              CREDIT_SCORE_B_EMAIL, CREDIT_SCORE_C_EMAIL,
                              CREDIT_SCORE_BIO, CREDIT_SCORE_LUCAS,
                              CREDIT_SCORE_LEANDRO, CREDIT_SCORE_MARCUS,
                              CREDIT_SCORE_SAULO, CREDIT_SCORE_ANDRE,
                              CREDIT_SCORE_DESIGN_1, CREDIT_SCORE_DESIGN_2,
                              CREDIT_61829708040, CREDIT_81008489042,
                              CREDIT_03724749007, CREDIT_04822044009,
                              CREDIT_28744717040, CREDIT_91307888038,
                              CREDIT_MESA_1, CREDIT_MESA_2, CREDIT_MESA_3,
                              CREDIT_MESA_4, CREDIT_65476005056,
                              CREDIT_37541438081)
from payloads.relatories import (ID_04741471004, ID_RESPONSE_04741471004,
                                 ID_19924764030, ID_RESPONSE_19924764030,
                                 ID_23158907054, ID_RESPONSE_23158907054,
                                 ID_33176053088, ID_RESPONSE_33176053088,
                                 ID_15260070020, ID_RESPONSE_15260070020,
                                 ID_LUCAS, ID_RESPONSE_LUCAS, ID_LEANDRO,
                                 ID_RESPONSE_LEANDRO, ID_SAULO,
                                 ID_RESPONSE_SAULO, ID_ANDRE, ID_RESPONSE_ANDRE,
                                 ID_BIO, ID_RESPONSE_BIO, ID_81008489042,
                                 ID_RESPONSE_81008489042,
                                 ID_04741471004_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_04741471004_DOCUMENTOSCOPIA,
                                 ID_19924764030_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_19924764030_DOCUMENTOSCOPIA,
                                 ID_23158907054_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_23158907054_DOCUMENTOSCOPIA,
                                 ID_BIO_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_BIO_DOCUMENTOSCOPIA,
                                 ID_33176053088_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_33176053088_DOCUMENTOSCOPIA,
                                 ID_15260070020_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_15260070020_DOCUMENTOSCOPIA,
                                 ID_LUCAS_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_LUCAS_DOCUMENTOSCOPIA,
                                 ID_LEANDRO_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_LEANDRO_DOCUMENTOSCOPIA,
                                 ID_SAULO_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_SAULO_DOCUMENTOSCOPIA,
                                 ID_ANDRE_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_ANDRE_DOCUMENTOSCOPIA,
                                 ID_61829708040, ID_RESPONSE_61829708040,
                                 ID_61829708040_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_61829708040_DOCUMENTOSCOPIA,
                                 ID_81008489042_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_81008489042_DOCUMENTOSCOPIA,
                                 ID_28744717040, ID_RESPONSE_28744717040,
                                 ID_28744717040_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_28744717040_DOCUMENTOSCOPIA,
                                 ID_91307888038, ID_RESPONSE_91307888038,
                                 ID_91307888038_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_91307888038_DOCUMENTOSCOPIA,
                                 ID_65476005056_CNH,
                                 ID_RESPONSE_65476005056_CNH, ID_65476005056_RG,
                                 ID_RESPONSE_65476005056_RG,
                                 ID_65476005056_DOCUMENTOSCOPIA,
                                 ID_RESPONSE_65476005056_DOCUMENTOSCOPIA)

from payloads.linx_nf import (NF_04741471004, POST_HEALTH_LINX_)

from payloads.recupera import (RECUPERA_ConsultarDividaEmAberto,
                               RECUPERA_ConsultarParcelamentos, RECUPERA_WSDL)

from payloads.sorocred import (SOROCRED_WSDL, PARTNER_CONTRACT_ID_04741471004,
                               PARTNER_CONTRACT_ID_ANDRE)

MOCK_API_URL = 'https://mock-server.qa-titan.lendico.net.br'
MAPPINGS = '__admin/mappings'
head = {'content-type': 'application/json'}


def make_credit_requests():
    all_credits = [
        CREDIT_04741471004, CREDIT_28157122023, CREDIT_72703627009,
        CREDIT_23158907054, CREDIT_19924764030, CREDIT_SCORE_A, CREDIT_SCORE_B,
        CREDIT_SCORE_C, CREDIT_SCORE_D, CREDIT_SCORE_E, CREDIT_SCORE_A_EMAIL,
        CREDIT_SCORE_B_EMAIL, CREDIT_SCORE_C_EMAIL, CREDIT_SCORE_BIO,
        CREDIT_SCORE_LUCAS, CREDIT_SCORE_LEANDRO, CREDIT_SCORE_MARCUS,
        CREDIT_SCORE_SAULO, CREDIT_SCORE_ANDRE, CREDIT_SCORE_DESIGN_1,
        CREDIT_SCORE_DESIGN_2, CREDIT_61829708040, CREDIT_81008489042,
        CREDIT_03724749007, CREDIT_04822044009, CREDIT_28744717040,
        CREDIT_91307888038, CREDIT_MESA_1, CREDIT_MESA_2, CREDIT_MESA_3,
        CREDIT_MESA_4, CREDIT_65476005056, CREDIT_37541438081
    ]
    for credit in all_credits:
        r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(credit), headers=head)
        print(f'Status Code - {r.status_code}\n Response - {r.text}')


def make_idwall_requests():
    all_relatories = [
        ID_04741471004, ID_19924764030, ID_BIO, ID_23158907054,
        ID_RESPONSE_04741471004, ID_RESPONSE_19924764030,
        ID_RESPONSE_23158907054, ID_RESPONSE_BIO, ID_33176053088,
        ID_RESPONSE_33176053088, ID_15260070020, ID_RESPONSE_15260070020,
        ID_LUCAS, ID_RESPONSE_LUCAS, ID_LEANDRO, ID_RESPONSE_LEANDRO, ID_SAULO,
        ID_RESPONSE_SAULO, ID_ANDRE, ID_RESPONSE_ANDRE,
        ID_04741471004_DOCUMENTOSCOPIA, ID_RESPONSE_04741471004_DOCUMENTOSCOPIA,
        ID_19924764030_DOCUMENTOSCOPIA, ID_RESPONSE_19924764030_DOCUMENTOSCOPIA,
        ID_23158907054_DOCUMENTOSCOPIA, ID_RESPONSE_23158907054_DOCUMENTOSCOPIA,
        ID_BIO_DOCUMENTOSCOPIA, ID_RESPONSE_BIO_DOCUMENTOSCOPIA,
        ID_33176053088_DOCUMENTOSCOPIA, ID_RESPONSE_33176053088_DOCUMENTOSCOPIA,
        ID_15260070020_DOCUMENTOSCOPIA, ID_RESPONSE_15260070020_DOCUMENTOSCOPIA,
        ID_LUCAS_DOCUMENTOSCOPIA, ID_RESPONSE_LUCAS_DOCUMENTOSCOPIA,
        ID_LEANDRO_DOCUMENTOSCOPIA, ID_RESPONSE_LEANDRO_DOCUMENTOSCOPIA,
        ID_SAULO_DOCUMENTOSCOPIA, ID_RESPONSE_SAULO_DOCUMENTOSCOPIA,
        ID_ANDRE_DOCUMENTOSCOPIA, ID_RESPONSE_ANDRE_DOCUMENTOSCOPIA,
        ID_61829708040, ID_RESPONSE_61829708040, ID_61829708040_DOCUMENTOSCOPIA,
        ID_RESPONSE_61829708040_DOCUMENTOSCOPIA, ID_81008489042_DOCUMENTOSCOPIA,
        ID_RESPONSE_81008489042_DOCUMENTOSCOPIA, ID_81008489042,
        ID_RESPONSE_81008489042, ID_28744717040, ID_RESPONSE_28744717040,
        ID_28744717040_DOCUMENTOSCOPIA, ID_RESPONSE_28744717040_DOCUMENTOSCOPIA,
        ID_91307888038, ID_RESPONSE_91307888038, ID_91307888038_DOCUMENTOSCOPIA,
        ID_RESPONSE_91307888038_DOCUMENTOSCOPIA, ID_65476005056_RG,
        ID_65476005056_CNH, ID_RESPONSE_65476005056_RG,
        ID_RESPONSE_65476005056_CNH, ID_65476005056_DOCUMENTOSCOPIA,
        ID_RESPONSE_65476005056_DOCUMENTOSCOPIA
    ]
    for relatorie in all_relatories:
        r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(relatorie),
                 headers=head)
        print(f'Status Code - {r.status_code}\n Response - {r.text}')


def make_callback_url_linx():
    r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(CALLBACK_URL),
             headers=head)
    print(f'Status Code - {r.status_code}\n Response - {r.text}')


def clean_mappings():
    r = get(f'{MOCK_API_URL}/{MAPPINGS}', headers=head)
    mappings = loads(r.text)
    mappings_id = [val_id['id'] for val_id in mappings['mappings']]
    for mapp_id in mappings_id:
        delete(f'{MOCK_API_URL}/{MAPPINGS}/{mapp_id}', headers=head)
    print(f'Mappings cleaned.')


def make_nf_linx():
    r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(NF_04741471004),
             headers=head)
    print(f'Status Code - {r.status_code}\n Response - {r.text}')


def make_health_linx():
    r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(POST_HEALTH_LINX_),
             headers=head)
    print(f'Status Code - {r.status_code}\n Response - {r.text}')


def get_quantity_mapps():
    r = get(f'{MOCK_API_URL}/{MAPPINGS}', headers=head)
    mappings = loads(r.text)
    print(f"{mappings['meta']['total']} Configured Mapps.")


# def wsdl():
#     r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(RECUPERA_WSDL),
#              headers=head)
#     print(f'Status Code - {r.status_code}\n Response - {r.text}')


# def consultar_divida_em_aberto():
#     r = post(f'{MOCK_API_URL}/{MAPPINGS}',
#              data=dumps(RECUPERA_ConsultarDividaEmAberto), headers=head)
#     print(f'Status Code - {r.status_code}\n Response - {r.text}')


# def consultar_parcelamentos():
#     r = post(f'{MOCK_API_URL}/{MAPPINGS}',
#              data=dumps(RECUPERA_ConsultarParcelamentos), headers=head)
#     print(f'Status Code - {r.status_code}\n Response - {r.text}')


def create_sorocred_services_env():
    items = [SOROCRED_WSDL, PARTNER_CONTRACT_ID_04741471004,
             PARTNER_CONTRACT_ID_ANDRE]
    for item in items:
        r = post(f'{MOCK_API_URL}/{MAPPINGS}', data=dumps(item), headers=head)
        print(f'Status Code - {r.status_code}\n Response - {r.text}')


if __name__ == '__main__':
    get_quantity_mapps()
    clean_mappings()
    make_credit_requests()
    make_idwall_requests()
    make_callback_url_linx()
    make_nf_linx()
    make_health_linx()
    create_sorocred_services_env()
    print('--------------------------------')
    get_quantity_mapps()
